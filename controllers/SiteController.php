<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;

//form
use app\models\FormContact;
use app\models\FormRegister;


//tabel
use app\models\Messages;
use app\models\Usersign;
use app\models\JenisProduk;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //ayam potong
        $ayampotong1 = JenisProduk::find(['kategori'=>'D'])->limit(3)->offset(0)
            ->orderBy(['gambar'=>SORT_DESC])->all();
        $ayampotong2 = JenisProduk::find(['kategori'=>'D'])->limit(3)->offset(3)
            ->orderBy(['gambar'=>SORT_DESC])->all();

        //ayam petelur
        $ayamtelur1 = JenisProduk::find()->where(['kategori'=>'T'])->limit(3)->all();

        return $this->render('index',[
            'ayampotong1'=>$ayampotong1,
            'ayampotong2'=>$ayampotong2,
            'ayamtelur1'=>$ayamtelur1,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new FormContact();
        if($model->load(Yii::$app->request->post())){
            $tabel = new Messages();
            $tabel->date_create = date('Y-m-d H:i:s');
            $tabel->fullname    = $model->fullname;
            $tabel->email       = $model->email;
            $tabel->message     = $model->message;
            
            if($tabel->save()){
                Yii::$app->session->setFlash('success', "Terimakasih sudah memberikan kami kritik dan sarannya.");
                $this->redirect('contact');
            }
        } 
        else {
            return $this->render('contact', ['model'=>$model]);
        }
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays register page.
     *
     * @return string
     */
    public function actionRegister()
    {
        $model = new FormRegister();
        if($model->load(Yii::$app->request->post())){
            $tabel = new Usersign();
            $tabel->date_create = date('Y-m-d H:i:s');
            $tabel->firstname   = $model->firstname;
            $tabel->lastname    = $model->lastname;
            $tabel->email       = $model->email;
            $tabel->password    = $model->password;
            
            if($tabel->save()){
                Yii::$app->session->setFlash('success', "Rergister anda berhasil, silahkan langsung login.");
                $this->redirect('register');
            }
        } 
        else {
            return $this->render('register', ['model'=>$model]);
        }
    }

    /**
     * Displays login page.
     *
     * @return string
     */
    public function actionLogin()
    {
        return $this->render('login');
    }

    /**
     * Displays faqs page.
     *
     * @return string
     */
    public function actionFaqs()
    {
        return $this->render('faqs');
    }

    public function actionMycart()
    {
        return $this->render('mycart');
    }

    public function actionAyampotong()
    {
        return $this->render('ayampotong');
    }

    public function actionAyampetelur()
    {
        return $this->render('ayampetelur');
    }

    public function actionAyammasakan()
    {
        return $this->render('ayammasakan');
    }

    public function actionProduct()
    {
        $dataMenu1 = JenisProduk::find()->where(['kategori'=>'D'])->orderBy(['gambar'=>SORT_DESC])->all();
        $dataMenu2 = JenisProduk::find()->where(['kategori'=>'T'])->orderBy(['gambar'=>SORT_DESC])->all();

        $dataproduk1 = JenisProduk::find()->where(['kategori'=>'D'])->orWhere(['kategori'=>'T'])
            ->orderBy(['gambar'=>SORT_DESC])->limit(3)->offset(0)->all();

        $dataproduk2 = JenisProduk::find()->where(['kategori'=>'D'])->orWhere(['kategori'=>'T'])
            ->orderBy(['gambar'=>SORT_DESC])->limit(3)->offset(3)->all();
        
        return $this->render('product', [
            'dataproduk1'=>$dataproduk1,
            'dataproduk2'=>$dataproduk2,
            'dataMenu1'=>$dataMenu1,
            'dataMenu2'=>$dataMenu2,
        ]);
    }

    public function actionDetailproduk($id)
    {
        $dataproduk = JenisProduk::find()->where(['kategori'=>'D'])->orWhere(['kategori'=>'T'])
            ->orderBy(['gambar'=>SORT_DESC])->limit(4)->offset(0)->all();
        
        $detailproduk = JenisProduk::find()->where(['idjenisproduk'=>$id])->one();   

        return $this->render('detailproduk', [
            'dataproduk'=>$dataproduk,
            'detailproduk'=>$detailproduk
        ]);
    }
}
