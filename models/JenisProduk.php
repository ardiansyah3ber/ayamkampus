<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_produk".
 *
 * @property integer $idjenisproduk
 * @property string $namaproduk
 * @property double $hargaproduk
 * @property string $kategori
 * @property string $gambar
 */
class JenisProduk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_produk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['namaproduk', 'hargaproduk', 'kategori', 'gambar'], 'required'],
            [['hargaproduk'], 'number'],
            [['kategori'], 'string'],
            [['namaproduk'], 'string', 'max' => 20],
            [['gambar'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idjenisproduk' => 'Idjenisproduk',
            'namaproduk' => 'Namaproduk',
            'hargaproduk' => 'Hargaproduk',
            'kategori' => 'Kategori',
            'gambar' => 'Gambar',
        ];
    }
}
