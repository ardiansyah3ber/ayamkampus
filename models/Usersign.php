<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usersign".
 *
 * @property integer $id
 * @property string $date_create
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $last_login
 */
class Usersign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usersign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'last_login'], 'safe'],
            [['firstname', 'lastname'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Date Create',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'password' => 'Password',
            'last_login' => 'Last Login',
        ];
    }
}
