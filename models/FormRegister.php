<?php
namespace app\models;

use Yii;
use yii\base\Model;

class FormRegister extends Model 
{
	public $firstname;
    public $lastname;
    
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['firstname','lastname','email','password'], 'required'],
            [['firstname'], 'string', 'max' => 15],
            [['lastname'], 'string', 'max' => 15],
            ['email', 'email'],
        ];
    }
}