<?php
namespace app\models;

use Yii;
use yii\base\Model;

class FormContact extends Model 
{
	public $fullname;
    public $email;
    public $message;

    public function rules()
    {
        return [
            [['fullname','email','message'], 'required'],
            ['email', 'email'],
			[['message'], 'string', 'max' => 300],
        ];
    }
}