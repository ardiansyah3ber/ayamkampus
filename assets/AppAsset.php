<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        
        // 'tema/css/bootstrap.css',
        // 'tema/css/style.css',
        // 'tema/css/font-awesome.css',
        // 'tema/css/skdslider.css'
    ];
    public $js = [
        // 'tema/js/jquery-1.11.1.min.js',
        // 'tema/js/move-top.js',
        // 'tema/js/easing.js',
        // 'tema/js/bootstrap.min.js',
        // 'tema/js/minicart.min.js',
        // 'tema/js/skdslider.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
