-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: promoayam
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jenis_produk`
--

DROP TABLE IF EXISTS `jenis_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_produk` (
  `idjenisproduk` int(11) NOT NULL AUTO_INCREMENT,
  `namaproduk` varchar(20) NOT NULL,
  `hargaproduk` double NOT NULL,
  `kategori` enum('T','D') NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`idjenisproduk`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_produk`
--

LOCK TABLES `jenis_produk` WRITE;
/*!40000 ALTER TABLE `jenis_produk` DISABLE KEYS */;
INSERT INTO `jenis_produk` VALUES (1,'Telur Ayam',20400,'T','pet1.jpg'),(2,'Ayam Afkir',35000,'T','pet2.jpg'),(3,'Ayam Utuh',29500,'D',''),(4,'Ayam Potong',27000,'D',''),(5,'Ayam Pejantan',30000,'D',''),(6,'Paha Ayam',29000,'D','pot14.jpg'),(7,'Dada Ayam',29000,'D','pot6.jpg'),(8,'Daging Fillet',38000,'D',''),(9,'Sayap Ayam',29000,'D',''),(10,'Punggung Ayam',19000,'D',''),(11,'Kepala Ayam',8000,'D',''),(12,'Kaki Ayam',21000,'D','pot12.jpg'),(13,'Usus Ayam',18000,'D','pot5.jpg'),(14,'Ati Ampela',30000,'D','pot1.jpg'),(15,'Kulit Ayam',16000,'D','pot4.jpg'),(16,'Jantung Ayam',16000,'D',''),(17,'Tulang Ayam',8000,'D',''),(18,'Ayam Afkir',35000,'T','pet3.jpg');
/*!40000 ALTER TABLE `jenis_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `fullname` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `message` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modal_daging`
--

DROP TABLE IF EXISTS `modal_daging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modal_daging` (
  `idmodaldaging` int(11) NOT NULL AUTO_INCREMENT,
  `jumbibitayam` int(11) NOT NULL,
  `hargabibitayam` double NOT NULL,
  `hargapakan` double NOT NULL,
  `listrik` double NOT NULL,
  `obat` double NOT NULL,
  `periode` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `sisadaging` int(11) NOT NULL,
  PRIMARY KEY (`idmodaldaging`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modal_daging`
--

LOCK TABLES `modal_daging` WRITE;
/*!40000 ALTER TABLE `modal_daging` DISABLE KEYS */;
INSERT INTO `modal_daging` VALUES (4,300,7000,3000000,200000,1000000,'D2017.12-4','2017-12-30',500),(5,100,7000,1000000,200000,330000,'D2017.12-5','2017-12-30',135);
/*!40000 ALTER TABLE `modal_daging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modal_telur`
--

DROP TABLE IF EXISTS `modal_telur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modal_telur` (
  `idmodaltelur` int(11) NOT NULL AUTO_INCREMENT,
  `jumbibitayam` int(11) NOT NULL,
  `hargabibitayam` double NOT NULL,
  `hargapakan` double NOT NULL,
  `periode` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `sisatelur` int(11) NOT NULL,
  PRIMARY KEY (`idmodaltelur`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modal_telur`
--

LOCK TABLES `modal_telur` WRITE;
/*!40000 ALTER TABLE `modal_telur` DISABLE KEYS */;
INSERT INTO `modal_telur` VALUES (4,300,50000,2880000,'T2017.12-4','2017-12-01',495),(5,100,50000,960000,'T2017.12-5','2017-11-01',100),(6,100,50000,960000,'T2017.12-6','2017-12-30',34);
/*!40000 ALTER TABLE `modal_telur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawai` (
  `idpegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `umur` int(11) NOT NULL,
  `jeniskelamin` enum('L','P') NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `notelepon` varchar(15) NOT NULL,
  PRIMARY KEY (`idpegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pegawai`
--

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;
INSERT INTO `pegawai` VALUES (5,'Lina Meritha',20,'P','Jl Kutisari Selatan Gg IVa no.25','083849317760'),(6,'Ahmad Ardiansyah',21,'L','Jl Sememu Ketewel Pasirian Lumajang','082849317760');
/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pegawailogin`
--

DROP TABLE IF EXISTS `pegawailogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawailogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpegawai` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idpegawai` (`idpegawai`),
  CONSTRAINT `pegawailogin_ibfk_1` FOREIGN KEY (`idpegawai`) REFERENCES `pegawai` (`idpegawai`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pegawailogin`
--

LOCK TABLES `pegawailogin` WRITE;
/*!40000 ALTER TABLE `pegawailogin` DISABLE KEYS */;
INSERT INTO `pegawailogin` VALUES (1,6,'ardiansyah','ardiansyah','2018-01-09 14:04:56');
/*!40000 ALTER TABLE `pegawailogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtransaksi` varchar(20) NOT NULL,
  `atasnama` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `idpegawai` int(11) NOT NULL,
  `total` double NOT NULL,
  `totalkg` int(11) NOT NULL,
  `bayar` double NOT NULL,
  `kembali` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksi`
--

LOCK TABLES `transaksi` WRITE;
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
INSERT INTO `transaksi` VALUES (1,'DT20171230-6','Lina Meritha','2017-12-30 18:58:37',6,204000,10,210000,6000),(2,'DT20171230-7','Ardiansyah','2017-12-30 19:00:07',6,408000,20,410000,2000),(3,'DT20171230-8','Wahyu Izzudin','2017-12-30 19:07:00',6,435000,15,450000,15000),(4,'DT20171230-9','Dimas Andre DW','2017-12-30 19:15:05',6,1330000,35,1350000,20000),(5,'DT20171230-10','Arif S','2017-12-29 19:45:23',6,102000,5,110000,8000),(6,'DT20171230-11','Ariel','2017-12-29 19:45:48',6,102000,5,110000,8000),(7,'DT20171230-12','Didin','2017-12-28 20:52:52',6,244800,12,250000,5200),(8,'DT20171230-13','Dudun','2017-12-28 20:56:14',6,448800,22,450000,1200),(9,'DT20171230-14','Udin Sedunia','2017-12-27 21:03:15',6,367200,18,400000,32800),(10,'DT20171230-15','Sudiro Tora','2017-12-27 21:07:29',6,673200,33,680000,6800),(11,'DT20171230-16','Mb. Linthung','2017-12-27 21:25:16',6,1020000,50,1020000,0),(12,'DT20171230-17','Diniatul Umami','2017-12-28 21:26:55',6,428400,21,430000,1600),(13,'DT20171230-18','Ando','2017-12-29 21:27:47',6,1020000,50,1020000,0),(14,'DT20171230-19','Ali Rodhi','2017-12-30 21:28:17',6,510000,25,510000,0),(15,'DT20171230-20','Sugianto','2017-12-29 21:57:13',6,290000,10,290000,0),(16,'DT20171230-21','Sugiarti','2017-12-29 21:57:34',6,380000,10,380000,0),(17,'DT20180104-22','Linthung','2018-01-04 17:22:17',6,435000,15,435000,0);
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaksidetail`
--

DROP TABLE IF EXISTS `transaksidetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaksidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtransaksi` varchar(20) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `jumkilo` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `periode` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idtransaksi` (`idtransaksi`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaksidetail`
--

LOCK TABLES `transaksidetail` WRITE;
/*!40000 ALTER TABLE `transaksidetail` DISABLE KEYS */;
INSERT INTO `transaksidetail` VALUES (24,'DT20171230-6','Telur',20400,10,204000,'T2017.12-4'),(25,'DT20171230-7','Telur',20400,20,408000,'T2017.12-5'),(28,'DT20171230-8','Paha Ayam',29000,10,290000,'D2017.12-4'),(29,'DT20171230-8','Dada Ayam',29000,5,145000,'D2017.12-4'),(30,'DT20171230-9','Daging Fillet',38000,35,1330000,'D2017.12-5'),(31,'DT20171230-10','Telur',20400,5,102000,'T2017.12-4'),(32,'DT20171230-11','Telur',20400,5,102000,'T2017.12-5'),(36,'DT20171230-12','Telur',20400,12,244800,'T2017.12-4'),(37,'DT20171230-13','Telur',20400,22,448800,'T2017.12-5'),(38,'DT20171230-14','Telur',20400,18,367200,'T2017.12-4'),(39,'DT20171230-15','Telur',20400,33,673200,'T2017.12-5'),(40,'DT20171230-16','Telur',20400,50,1020000,'T2017.12-6'),(41,'DT20171230-17','Telur',20400,21,428400,'T2017.12-6'),(42,'DT20171230-18','Telur',20400,50,1020000,'T2017.12-6'),(43,'DT20171230-19','Telur',20400,25,510000,'T2017.12-6'),(44,'DT20171230-20','Dada Ayam',29000,10,290000,'D2017.12-4'),(45,'DT20171230-21','Daging Fillet',38000,10,380000,'D2017.12-5'),(46,'DT20180104-22','Paha Ayam',29000,15,435000,'D2017.12-4');
/*!40000 ALTER TABLE `transaksidetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersign`
--

DROP TABLE IF EXISTS `usersign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `firstname` varchar(15) DEFAULT NULL,
  `lastname` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersign`
--

LOCK TABLES `usersign` WRITE;
/*!40000 ALTER TABLE `usersign` DISABLE KEYS */;
INSERT INTO `usersign` VALUES (1,'2017-12-27 11:04:16','Ahmad','Ardiansyah','ardiansyah3ber@gmail.com','terLalu7',NULL);
/*!40000 ALTER TABLE `usersign` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-11 10:08:39
