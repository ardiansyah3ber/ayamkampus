<?php 
use yii\helpers\Html;
use app\models\JenisProduk;

$queryMenuTelur  = JenisProduk::find()->where(['kategori'=>'T'])->orderBy(['gambar'=>SORT_DESC])->all();
$queryMenuDaging = JenisProduk::find()->where(['kategori'=>'D'])->orderBy(['gambar'=>SORT_DESC])->all();

$this->beginPage() 
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo Html::encode($this->title);?> | Ayam Kampus</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Super Market Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> 
        addEventListener("load", function() { 
            setTimeout(hideURLbar, 0); 
            }, false
        );
        
        function hideURLbar(){ window.scrollTo(0,1); } 
    </script>
    <!-- //for-mobile-apps -->
    <link href="<?php echo Yii::$app->homeUrl;?>tema/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo Yii::$app->homeUrl;?>tema/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- font-awesome icons -->
    <link href="<?php echo Yii::$app->homeUrl;?>tema/css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome icons -->
    <!-- js -->
    <script src="<?php echo Yii::$app->homeUrl;?>tema/js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="<?php echo Yii::$app->homeUrl;?>tema/js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo Yii::$app->homeUrl;?>tema/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
</head>

<?php $this->beginBody() ?>
<body>
    <!-- header -->
    <div class="agileits_header">
        <div class="container">
            <div class="w3l_offers">
                <p>SALE UP TO 70% OFF. USE CODE "SALE70%". <a href="<?php echo Yii::$app->homeUrl;?>">SHOP NOW</a></p>
            </div>
            <div class="agile-login">
                <ul>
                    <li><?php echo Html::a('Login', ['site/login']);?></li>
                    <li><?php echo Html::a('Register', ['site/register']);?></li>
                </ul>
            </div>
            <div class="product_list_header">  
                    <form action="#" method="post" class="last"> 
                        <input type="hidden" name="cmd" value="_cart">
                        <input type="hidden" name="display" value="1">
                        <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
                    </form>  
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="logo_products">
        <div class="container">
        <div class="w3ls_logo_products_left1">
                <ul class="phone_email">
                    <li>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        Order online or call us : (+62) 8233 4093 822
                    </li> 
                </ul>
            </div>
            <div class="w3ls_logo_products_left">
                <h1><a href="<?php echo Yii::$app->homeUrl;?>">Warung Bejan</a></h1>
            </div>
        <div class="w3l_search">
            <form action="#" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" aria-label="Left Align">
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
                <div class="clearfix"></div>
            </form>
        </div>
            
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //header -->

    <!-- navigation -->
    <div class="navigation-agileits">
        <div class="container">
            <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header nav_2">
                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div> 

            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="<?php echo Yii::$app->homeUrl;?>" class="act">Home</a>
                    </li>
                    <li><?php echo Html::a('Produk Terbaru', ['site/product']);?></li>   
                    <!-- Mega Menu -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ayam Potong<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Ayam Potong</h6>
                                        <?php foreach ($queryMenuDaging as $key) { 
                                            if($key->gambar != '') { ?>
                                            <li>
                                                <?php echo Html::a($key->namaproduk, ['site/detailproduk', 'id'=>$key->idjenisproduk]);?>
                                            </li>    
                                        <?php }} ?>
                                    </ul>
                                </div>  
                                
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ayam Petelur<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="multi-gd-img">
                                    <ul class="multi-column-dropdown">
                                        <h6>Jenis Telur</h6>
                                        <?php foreach ($queryMenuTelur as $key) { 
                                            if($key->gambar != '') { ?>
                                            <li>
                                                <?php echo Html::a($key->namaproduk, ['site/detailproduk', 'id'=>$key->idjenisproduk]);?>
                                            </li>    
                                        <?php }} ?>
                                    </ul>
                                </div>
                                
                            </div>
                        </ul>
                    </li>
                    
                    <li><?php echo Html::a('Tentang Kami', ['site/about']);?></li>
                    <li><?php echo Html::a('Kontak', ['site/contact']);?></li>
                </ul>
            </div>
            
            </nav>
        </div>
    </div>
    <!-- end navigation -->

    <?php echo $content; ?>

    <!-- //new -->
    <!-- //footer -->
    <div class="footer">
        <div class="container">
            <div class="w3_footer_grids">
                <div class="col-md-3 w3_footer_grid">
                    <h3>Contact</h3>
                    
                    <ul class="address">
                        <li>
                            <i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
                            Jl. Raya ITS Keputih Sukolilo, <span>Surabaya City.</span>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
                            <a href="mailto:linameritha84@gmail.com">admin@promoayam.com</a>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
                            +62 8233 4093 822
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 w3_footer_grid">
                    <h3>Information</h3>
                    <ul class="info"> 
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('About', ['site/about']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Contact', ['site/contact']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('FAQ\'S', ['site/faqs']);?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 w3_footer_grid">
                    <h3>Category</h3>
                    <ul class="info"> 
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Produk Terbaru', ['site/product']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Ayam Potong', ['site/ayampotong']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Ayam Petelur', ['site/ayampetelur']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Masakan Ayam', ['site/ayammasakan']);?>
                        </li>
                    </ul>
                </div>
                <div class="col-md-3 w3_footer_grid">
                    <h3>Profile</h3>
                    <ul class="info"> 
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('My Cart', ['site/mycart']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Login', ['site/login']);?>
                        </li>
                        <li>
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            <?php echo Html::a('Register', ['site/register']);?>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        
        <div class="footer-copy">
            
            <div class="container">
                <p>© 2017 Warung Bejan. All rights reserved | Design by <a href="http://linameritha27.it.student.pens.ac.id/">Lina Meritha</a></p>
            </div>
        </div>  
    </div>  

    <div class="footer-botm">
        <div class="container">
            <div class="w3layouts-foot">
                <ul>
                    <li><a href="http://facebook.com/" target="_blank" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="http://twitter.com/" target="_blank" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="http://dribble.com/" target="_blank" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="http://vimeo.com/" target="_blank" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="payment-w3ls">  
                <img src="images/card.png" alt=" " class="img-responsive">
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //footer -->   

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Yii::$app->homeUrl;?>tema/js/bootstrap.min.js"></script>

    <!-- top-header and slider -->
    <!-- here stars scrolling icon -->
    <script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
        */
                            
        $().UItoTop({ easingType: 'easeOutQuart' });
                            
        });
    </script>
    <!-- //here ends scrolling icon -->
    <script src="<?php echo Yii::$app->homeUrl;?>tema/js/minicart.min.js"></script>
    <script>
        // Mini Cart
        paypal.minicart.render({
            action: '#'
        });

        if (~window.location.search.indexOf('reset=true')) {
            paypal.minicart.reset();
        }
    </script>
    <!-- main slider-banner -->
    <script src="<?php echo Yii::$app->homeUrl;?>tema/js/skdslider.min.js"></script>
    <link href="<?php echo Yii::$app->homeUrl;?>tema/css/skdslider.css" rel="stylesheet">
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
                        
            jQuery('#responsive').change(function(){
              $('#responsive_wrapper').width(jQuery(this).val());
            });
            
        });
    </script>   
    <!-- //main slider-banner --> 
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>