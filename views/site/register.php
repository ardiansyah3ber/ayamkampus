<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Register Page</li>
		</ol>
	</div>
</div>
<!-- //breadcrumbs -->
<?php if (Yii::$app->session->hasFlash('success')): ?>
	<br>
	<div class="container">	
		<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<h4><i class="icon fa fa-check"></i>Saved!</h4>
			<?= Yii::$app->session->getFlash('success') ?>
		</div>
	</div>
<?php endif; ?>

<!-- register -->
<div class="register">
	<div class="container">
		<h2>Register Here</h2>
		<div class="login-form-grids">
			<h5>profile information</h5>
			<?php $form = ActiveForm::begin();?>
				<?php echo $form->field($model, 'firstname');?>
				<?php echo $form->field($model, 'lastname');?>
				
				<div class="register-check-box">
					<div class="check">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>Subscribe to Newsletter</label>
					</div>
				</div>

				<h6>Login information</h6>
				<?php echo $form->field($model, 'email');?>
				<?php echo $form->field($model, 'password');?>
				<div class="register-check-box">
					<div class="check">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>I accept the terms and conditions</label>
					</div>
				</div>
				<input type="submit" value="Register">
			<?php ActiveForm::end();?>
		</div>
		<div class="register-home">
			<a href="<?php echo Yii::$app->homeUrl;?>">Home</a>
		</div>
	</div>
</div>
<!-- //register -->
