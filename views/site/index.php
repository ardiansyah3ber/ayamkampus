<?php

/* @var $this yii\web\View */

$this->title = 'Home';
?>
<!-- main-slider -->
<ul id="demo1">
    <li>
        <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/3.jpg" alt="" />
        <!--Slider Description example-->
        <div class="slide-desc">
            <h3>Beli Ayam Potong Bisa Dengan Mudah Didapatkan</h3>
        </div>
    </li>
    <li>
        <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/8.jpg" alt="" />
        <div class="slide-desc">
            <h3>Ayam Potong Enak dan Lezat Sangat Menggoda</h3>
        </div>
    </li>
    <li>
        <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/12.jpg" alt="" />
          <div class="slide-desc">
            <h3>Telur Ayam Dengan Kualitas Tinggi dan Harga Terjangkau</h3>
        </div>
    </li>
    <li>
        <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/10.jpg" alt="" />
        <div class="slide-desc">
            <h3>Tersedia Ayam Afkir Untuk Menemani Weekend Anda</h3>
        </div>
    </li>
    <li>
        <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/7.jpg" alt="" />
        <div class="slide-desc">
            <h3>Dapatkan Hati Dengan Mudah Bisa Melalui Whatsapp</h3>
        </div>
    </li>
</ul>
<!-- //main-slider -->
<!-- //top-header and slider -->

<!-- top-brands -->
<div class="top-brands">
    <div class="container">
    <h2>Produk Warung Bejan</h2>
        <div class="grid_3 grid_5">
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">Produk Ayam Potong</a></li>
                    <li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Produk Ayam Petelur</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
                        <div class="agile-tp">
                            <h5>Produk Ayam Potong</h5>
                            <p class="w3l-ad">Menjual berbagai macam jenis bagian dari ayam potong yang higienis dan memuaskan</p>
                        </div>
                        <div class="agile_top_brands_grids">
                            <?php foreach ($ayampotong1 as $key) { ?>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/offer.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk">
                                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/<?php echo $key->gambar;?>" alt="" class="img-responsive"/>
                                                        </a>    
                                                        <p><?php echo $key->namaproduk;?></p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>
                                                            Rp <?php echo number_format($key->hargaproduk, 2, ',', '.');?> 
                                                            <span>
                                                                Rp <?php echo number_format($key->hargaproduk+5000, 2, ',','.');?>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Fortune Sunflower Oil" />
                                                                <input type="hidden" name="amount" value="20.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                           <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                         <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot14.jpg" alt="" class="img-responsive"/></a>     
                                                        <p>Paha Ayam</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp29.000,-<span>Rp32.000,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="basmati rise" />
                                                                <input type="hidden" name="amount" value="20.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block">
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot12.jpg" alt="" class="img-responsive"/></a>
                                                        <p>Kaki Ayam</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp21.000,-<span>Rp22.500,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Pepsi soft drink" />
                                                                <input type="hidden" name="amount" value="40.00" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="clearfix"> </div>
                        </div>

                        <div class="agile_top_brands_grids">
                            <?php foreach ($ayampotong2 as $key) { ?>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/offer.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/<?php echo $key->gambar;?>" alt="" class="img-responsive"/></a>  
                                                        <p><?php echo $key->namaproduk;?></p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>
                                                            Rp <?php echo number_format($key->hargaproduk, 2, ',', '.');?> 
                                                            <span>
                                                                Rp <?php echo number_format($key->hargaproduk+5000, 2, ',','.');?>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Fortune Sunflower Oil" />
                                                                <input type="hidden" name="amount" value="35.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                           <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot1.jpg" alt="" class="img-responsive"/></a>
                                                        <p>Ati Ampela</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp30.000,-<span>Rp31.500,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="basmati rise" />
                                                                <input type="hidden" name="amount" value="30.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        
                                        <div class="agile_top_brand_left_grid_pos">
                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block">
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot4.jpg" alt="" class="img-responsive"/></a>
                                                        <p>Kulit Ayam</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp16.000,-<span>Rp18.000,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Pepsi soft drink" />
                                                                <input type="hidden" name="amount" value="80.00" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="clearfix"> </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
                        <div class="agile-tp">
                            <h5>Produk Ayam Petelur</h5>
                            <p class="w3l-ad">Menjual telur dengan kualitas terbaik dan ayam afkir yang sangat besar.</p>
                        </div>
                        <div class="agile_top_brands_grids">
                            <?php foreach ($ayamtelur1 as $key) { ?>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                            <img src="<?php echo Yii::getAlias('@web');?>/tema/images/offer.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                         <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/<?php echo $key->gambar;?>" alt="" class="img-responsive"/></a> 
                                                        <p><?php echo $key->namaproduk;?></p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>
                                                            Rp <?php echo number_format($key->hargaproduk, 2, ',', '.');?> 
                                                            <span>
                                                                Rp <?php echo number_format($key->hargaproduk+5000, 2, ',','.');?>
                                                            </span>
                                                        </h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Fortune Sunflower Oil" />
                                                                <input type="hidden" name="amount" value="35.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <!-- <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                           <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block" >
                                                    <div class="snipcart-thumb">
                                                        <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pet1.jpg" alt="" class="img-responsive"/></a>   
                                                        <p>Telur Ayam</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp20.400,-<span>Rp21.500,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="basmati rise" />
                                                                <input type="hidden" name="amount" value="30.99" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 top_brand_left">
                                <div class="hover14 column">
                                    <div class="agile_top_brand_left_grid">
                                        <div class="agile_top_brand_left_grid_pos">
                                           <img src="<?php echo Yii::getAlias('@web');?>/tema/images/diskon.png" alt="" class="img-responsive"/>
                                        </div>
                                        <div class="agile_top_brand_left_grid1">
                                            <figure>
                                                <div class="snipcart-item block">
                                                    <div class="snipcart-thumb">
                                                         <a href="site/detailproduk"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pet3.jpg" alt="" class="img-responsive"/></a>
                                                        <p>Ayam Afkir</p>
                                                        <div class="stars">
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star blue-star" aria-hidden="true"></i>
                                                            <i class="fa fa-star gray-star" aria-hidden="true"></i>
                                                        </div>
                                                        <h4>Rp35.000<span>Rp37.000,-</span></h4>
                                                    </div>
                                                    <div class="snipcart-details top_brand_home_details">
                                                        <form action="#" method="post">
                                                            <fieldset>
                                                                <input type="hidden" name="cmd" value="_cart" />
                                                                <input type="hidden" name="add" value="1" />
                                                                <input type="hidden" name="business" value=" " />
                                                                <input type="hidden" name="item_name" value="Pepsi soft drink" />
                                                                <input type="hidden" name="amount" value="80.00" />
                                                                <input type="hidden" name="discount_amount" value="1.00" />
                                                                <input type="hidden" name="currency_code" value="USD" />
                                                                <input type="hidden" name="return" value=" " />
                                                                <input type="hidden" name="cancel_return" value=" " />
                                                                <input type="submit" name="submit" value="Add to cart" class="button" />
                                                            </fieldset>
                                                        </form>
                                                    </div>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
