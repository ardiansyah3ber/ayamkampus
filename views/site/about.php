<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">About</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!-- about -->
	<div class="about">
		<div class="container">
			<h3 class="w3_agile_header">About Us</h3>
			<div class="about-agileinfo w3layouts">
				<div class="col-md-8 about-wthree-grids grid-top">
					<h4>Warung Bejan</h4>
					<p class="top">Warung bejan merupakan sebuah tempat belanja yang menjual berbagai macam jenis ayam potong dan ayam petelur.
					Warung bejan menjual jenis ayam potong berupa paha, sayap, dada, kepala, ceker ayam, usus dan hati ayam. Selain itu,
					warung bejan juga menjual telur ayam dan ayam afkir (ayam yang sudah tidak dapat bertelur). Harga yang dijual pada Warung
					Bejan sangat terjangkau dan murah meriah. Pembelian bisa dipesan melalui online. Pengiriman hanya dilakukan via gojek
					dan menerima pembelian secara langsung pada Warung Bejan yang terletak di daerah Kutisari.</p>	
					<div class="about-w3agilerow">
						<div class="col-md-4 about-w3imgs">
							<img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot3.jpg" alt="" class="img-responsive" />
						</div>
						<div class="col-md-4 about-w3imgs">
							<img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pet1.jpg" alt="" class="img-responsive" />
						</div>
						<div class="col-md-4 about-w3imgs">
							<img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/pot11.jpg" alt="" class="img-responsive" />
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-4 about-wthree-grids">
					<div class="offic-time">
						<div class="time-top">
							<h4>Alamat :</h4>
						</div>
						<div class="time-bottom">
							<p>Jl Kutisari Selatan Gg IV a no.26 </p>
							<p>Tenggilis Mejoyo</p>
							<p>Surabaya</p>
							
						</div>
					</div>
					<div class="testi">
						<h3 class="w3_agile_header">Testimonial</h3>
						<!--//End-slider-script -->
						<script src="js/responsiveslides.min.js"></script>
						 <script>
							// You can also use "$(window).load(function() {"
							$(function () {
							  // Slideshow 5
							  $("#slider5").responsiveSlides({
								auto: true,
								pager: false,
								nav: true,
								speed: 500,
								namespace: "callbacks",
								before: function () {
								  $('.events').append("<li>before event fired.</li>");
								},
								after: function () {
								  $('.events').append("<li>after event fired.</li>");
								}
							  });
						
							});
						  </script>
						<div  id="top" class="callbacks_container">
							<ul class="rslides" id="slider5">
								<li>
									<div class="testi-slider">
										<h4>"MAKANAN YANG DIJUAL SANGAT LEZAT .</h4>
										<p>Ayam kremesnya mantab sekali, saya sangat suka yang ada mozarellanya.</p>
										<div class="testi-subscript">
											<p><a href="#">linameritha84@gmail.com,</a>Lina Meritha</p>
											<span class="w3-agilesub"> </span>
										</div>	
									</div>
								</li>
								<li>
									<div class="testi-slider">
										<h4>"MAKANAN YANG DIJUAL SANGAT LEZAT .</h4>
										<p>Ayam kremesnya mantab sekali, saya sangat suka yang ada mozarellanya.</p>
										<div class="testi-subscript">
											<p><a href="#">linameritha84@gmail.com,</a>Lina Meritha</p>
											<span class="w3-agilesub"> </span>
										</div>	
									</div>
								</li>
								<li>
									<div class="testi-slider">
										<h4>"MAKANAN YANG DIJUAL SANGAT LEZAT .</h4>
										<p>Ayam kremesnya mantab sekali, saya sangat suka yang ada mozarellanya.</p>
										<div class="testi-subscript">
											<p><a href="#">linameritha84@gmail.com,</a>Lina Meritha</p>
											<span class="w3-agilesub"> </span>
										</div>	
									</div>
								</li>
							</ul>	
						</div>
					</div>
				</div>	
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //about -->
	<!-- about-slid -->
	<div class="about-slid agileits-w3layouts"> 
		<div class="container">
			<div class="about-slid-info"> 
				<h2>Warung Bejan</h2>
				<p>Warung Bejan menjual banyak sekali makanan lezat yang higienis dan halal. Warung Bejan berada di seluruh dunia dan selalu ada di hati anda semua. </p>
			</div>
		</div>
	</div>
	<!-- //about-slid -->
	<!-- about-team -->
	<div class="about-team"> 
		<div class="container">
			<h3 class="w3_agile_header">Meet Our Team</h3>
			<div class="team-agileitsinfo">
				<div class="col-md-3 about-team-grids">
					<img src="<?php echo Yii::getAlias('@web');?>/tema/images/Ardi.png" alt="" class="img-responsive" />
					<div class="team-w3lstext">
						<h4><span>ARDIANSYAH,</span> Developer</h4>
						<p>Lahir di Kota Lumajang, tanggal 17 Januari 2017.</p>
					</div>
					<div class="social-icons caption"> 
						<ul>
							<li><a href="#" class="fa fa-facebook facebook"> </a></li>
							<li><a href="#" class="fa fa-twitter twitter"> </a></li>
							<li><a href="#" class="fa fa-google-plus googleplus"> </a></li> 
						</ul>
						<div class="clearfix"> </div>  
					</div>
				</div>
				<div class=" col-md-3 about-team-grids">
					<img src="<?php echo Yii::getAlias('@web');?>/tema/images/Arif.png" alt="" class="img-responsive" />
					<div class="team-w3lstext">
						<h4><span>ARIF RAHMAN S.,</span> Database Administration</h4>
						<p>Pemain sepak bola, pecinta olahraga futsal. Nama Panggilan Cahyok</p>
					</div>
					<div class="social-icons caption"> 
						<ul>
							<li><a href="#" class="fa fa-facebook facebook"> </a></li>
							<li><a href="#" class="fa fa-twitter twitter"> </a></li>
							<li><a href="#" class="fa fa-google-plus googleplus"> </a></li> 
						</ul>
						<div class="clearfix"> </div>  
					</div>
				</div>
				<div class="col-md-3 about-team-grids">
					<img src="<?php echo Yii::getAlias('@web');?>/tema/images/Lina.png" alt="" class="img-responsive" />
					<div class="team-w3lstext">
						<h4><span>LINA MERITHA,</span> Project Manager</h4>				
						<p>Paling imut dan berbadan kecil. Suka senyum hobby menulis.</p>
					</div>
					<div class="social-icons caption"> 
						<ul>
							<li><a href="https://web.facebook.com/linamrth" class="fa fa-facebook facebook"> </a></li>
							<li><a href="https://twitter.com/linameritha" class="fa fa-twitter twitter"> </a></li>
							<li><a href="https://plus.google.com/discover" class="fa fa-google-plus googleplus"> </a></li> 
						</ul>
						<div class="clearfix"> </div>  
					</div>
				</div>
				<div class="col-md-3 about-team-grids">
					<img src="<?php echo Yii::getAlias('@web');?>/tema/images/Novia.png" alt="" class="img-responsive" />
					<div class="team-w3lstext">
						<h4><span>NOVIA RAHAYU K.,</span> Documentation</h4>
						<p>Suka mengaji, hobby menyanyi. Pejuang hebat.</p>
					</div>
					<div class="social-icons caption"> 
						<ul>
							<li><a href="#" class="fa fa-facebook facebook"> </a></li>
							<li><a href="#" class="fa fa-twitter twitter"> </a></li>
							<li><a href="#" class="fa fa-google-plus googleplus"> </a></li> 
						</ul>
						<div class="clearfix"> </div>  
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //about-team -->
