<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Produk Terbaru Kami';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="<?php echo Yii::$app->homeUrl;?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Produk Terbaru</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!--- products -->
	<div class="products">
		<div class="container">
			<div class="col-md-4 products-left">
				<div class="categories">
					<h2>Produk Terbaru</h2>
					<ul class="cate">
						<li><a href="detailproduk/"><i class="fa fa-arrow-right" aria-hidden="true"></i>Ayam Potong</a></li>
							<ul>
							<?php foreach ($dataMenu1 as $key) { ?>
								<li>
									<a href="detailproduk/<?php echo $key->idjenisproduk;?>"><i class="fa fa-arrow-right" aria-hidden="true"></i>
									<?php echo $key->namaproduk;?>
									</a>
								</li>
							<?php } ?>
							</ul>
						
						<li><a href="detailproduk"><i class="fa fa-arrow-right" aria-hidden="true"></i>Ayam Petelur</a></li>
							<ul>
								<?php foreach ($dataMenu2 as $key) { ?>
								<li>
									<a href="detailproduk/<?php echo $key->idjenisproduk;?>"><i class="fa fa-arrow-right" aria-hidden="true"></i>
									<?php echo $key->namaproduk;?>
									</a>
								</li>
							<?php } ?>
							</ul>
					</ul>
				</div>																																												
			</div>
			<div class="col-md-8 products-right">
				<div class="agile_top_brands_grids">
					<?php foreach ($dataproduk1 as $key) { ?>					
					<div class="col-md-4 top_brand_left">
						<div class="hover14 column">
							<div class="agile_top_brand_left_grid">
								<div class="agile_top_brand_left_grid_pos">
									 <img src="<?php echo Yii::getAlias('@web');?>/tema/images/offer.png" alt="" class="img-responsive"/>
								</div>
								<div class="agile_top_brand_left_grid1">
									<figure>
										<div class="snipcart-item block">
											<div class="snipcart-thumb">
												<a href="detailproduk/<?php echo $key->idjenisproduk;?>"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/<?php echo $key->gambar;?>" alt="" class="img-responsive"/></a>		
												<p><?php echo $key->namaproduk;?></p>
												<h4>
	                                                Rp <?php echo number_format($key->hargaproduk, 2, ',', '.');?> 
	                                                <br>
	                                                <span>
	                                                    Rp <?php echo number_format($key->hargaproduk+5000, 2, ',','.');?>
	                                                </span>
	                                            </h4>
											</div>
											<div class="snipcart-details top_brand_home_details">
												<form action="#" method="post">
													<fieldset>
														<input type="hidden" name="cmd" value="_cart">
														<input type="hidden" name="add" value="1">
														<input type="hidden" name="business" value=" ">
														<input type="hidden" name="item_name" value="Fortune Sunflower Oil">
														<input type="hidden" name="amount" value="35.99">
														<input type="hidden" name="discount_amount" value="1.00">
														<input type="hidden" name="currency_code" value="USD">
														<input type="hidden" name="return" value=" ">
														<input type="hidden" name="cancel_return" value=" ">
														<input type="submit" name="submit" value="Add to cart" class="button">
													</fieldset>
												</form>
											</div>
										</div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="clearfix"> </div>
				</div>
				<div class="agile_top_brands_grids">
					<?php foreach ($dataproduk2 as $key) { ?>
					<div class="col-md-4 top_brand_left">
						<div class="hover14 column">
							<div class="agile_top_brand_left_grid">
								<div class="agile_top_brand_left_grid_pos">
									 <img src="<?php echo Yii::getAlias('@web');?>/tema/images/offer.png" alt="" class="img-responsive"/>
								</div>
								<div class="agile_top_brand_left_grid1">
									<figure>
										<div class="snipcart-item block">
											<div class="snipcart-thumb">
												<a href="detailproduk/<?php echo $key->idjenisproduk;?>"><img src="<?php echo Yii::getAlias('@web');?>/tema/images/ayam/<?php echo $key->gambar;?>" alt="" class="img-responsive"/> </a>		
												<p><?php echo $key->namaproduk;?></p>
												<h4>
                                                    Rp <?php echo number_format($key->hargaproduk, 2, ',', '.');?> 
                                                    <br>
                                                    <span>
                                                        Rp <?php echo number_format($key->hargaproduk+5000, 2, ',','.');?>
                                                    </span>
                                                </h4>
											</div>
											<div class="snipcart-details top_brand_home_details">
												<form action="#" method="post">
													<fieldset>
														<input type="hidden" name="cmd" value="_cart">
														<input type="hidden" name="add" value="1">
														<input type="hidden" name="business" value=" ">
														<input type="hidden" name="item_name" value="Fortune Sunflower Oil">
														<input type="hidden" name="amount" value="35.99">
														<input type="hidden" name="discount_amount" value="1.00">
														<input type="hidden" name="currency_code" value="USD">
														<input type="hidden" name="return" value=" ">
														<input type="hidden" name="cancel_return" value=" ">
														<input type="submit" name="submit" value="Add to cart" class="button">
													</fieldset>
												</form>
											</div>
										</div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!--- products -->