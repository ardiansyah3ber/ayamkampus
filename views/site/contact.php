<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contact';
?>
<div class="breadcrumbs">
	<div class="container">
		<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
			<li><a href="<?php echo Yii::$app->homeUrl;?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
			<li class="active">Kontak Kami</li>
		</ol>
	</div>
</div>

<?php if (Yii::$app->session->hasFlash('success')): ?>
	<br>
	<div class="container">	
		<div class="alert alert-success alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			<h4><i class="icon fa fa-check"></i>Saved!</h4>
			<?= Yii::$app->session->getFlash('success') ?>
		</div>
	</div>
<?php endif; ?>

<div class="about">
	<div class="w3_agileits_contact_grids">
		<div class="col-md-6 w3_agileits_contact_grid_left">
			<div class="agile_map">
				<iframe src="https://www.google.co.id/maps/place/Politeknik+Elektronika+Negeri+Surabaya/@-7.2756305,112.791546,17z/data=!4m12!1m6!3m5!1s0x2dd7fa10c738f891:0xecfa2529c4987c53!2sPoliteknik+Elektronika+Negeri+Surabaya!8m2!3d-7.2756358!4d112.7937347!3m4!1s0x2dd7fa10c738f891:0xecfa2529c4987c53!8m2!3d-7.2756358!4d112.7937347?hl=id&hl=id" style="border:0"></iframe>
			</div>
			<div class="agileits_w3layouts_map_pos">
				<div class="agileits_w3layouts_map_pos1">
					<h3>Kontak Info</h3>
					<p>Warung Bejan</p>
					<p>Jl. Raya ITS Sukolilo, Surabaya</p>
					<ul class="wthree_contact_info_address">
						<li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@example.com">admin@promoayam.com</a></li>
						<li><i class="fa fa-phone" aria-hidden="true"></i>+(62) 8233 4093 822</li>
					</ul>
					<div class="w3_agile_social_icons w3_agile_social_icons_contact">
						<ul>
							<li><a href="#" class="icon icon-cube agile_facebook"></a></li>
							<li><a href="#" class="icon icon-cube agile_rss"></a></li>
							<li><a href="#" class="icon icon-cube agile_t"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 w3_agileits_contact_grid_right">
			<h2 class="w3_agile_header">Kirimkan Pesan <span> Anda Ke Kami</span></h2>

			<?php $form = ActiveForm::begin();?>
				<?php echo $form->field($model, 'fullname')->label('Full Name');?>
				<?php echo $form->field($model, 'email')->label('Email');?>
				<?php echo $form->field($model, 'message')->textArea(['rows'=>'3','style'=>'margin: 0px']);?>
				<?= Html::submitButton('Submit', ['class'=>'btn btn-success']); ?>
			<?php ActiveForm::end();?>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>